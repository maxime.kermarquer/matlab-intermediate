%% When related pieces of data have different data types, you can keep them together in a cell array.
% Each cell contains a piece of data. To refer to elements of a cell array, use array indexing.
% You can index into a cell array using smooth parentheses, (), and into the contents of cells using curly braces, {}.


%% Cell array creation

C = {1,2,3; 'text',rand(5,10,2),{11; 22; 33}}

%% Access elements of cell array
C(1,3)
C{1,3}
C{2,3}
C{2,3}(2)
C{2,3}{2}


Temp_20220816 = {'2022-08-16',[17 30 25]}
Temp_20220817 = {'2022-08-17', 28}
Temp_20220818 = {'2022-08-18', [15 28 32 29 25]}

Temp_august = [Temp_20220816 ; Temp_20220817 ; Temp_20220818 ]

% Access by index
Temp_august{:,1}
Temp_august{:,2}

% Compute the median of all temperatures
[Temp_august{:,2}]; % aggregate all data in the same vector
median([Temp_august{:,2}])
