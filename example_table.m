%% table arrays store column-oriented or tabular data, such as columns from a text file or spreadsheet.
% Tables store each piece of column-oriented data in a variable.
% Table variables can have different data types and sizes as long as all variables have the same number of rows.

%% Vectors
LastName = ["Sanchez"; "Johnson"; "Zhang"; "Diaz"; "Brown"];  
Age = [38; 43; 38; 40; 49]; 
Smoker = [true; false; true; true; false]; 
Height = [1.55; 1.90; 1.65; 1.50; 1.70]; 
Weight = [50; 90; 70; 45; 80]; 
BloodPressure = [124 93;  109 77;  125 83;  117 75;  122 80]; 

%% Table creation
% With table function from vectors
% The column names will be the variable namess
patients = table(LastName,Age,Smoker,Height,Weight,BloodPressure)

% An another way to do this
patients = table()
patients.Name = LastName 
patients.Age = Age
patients.Smoker = Smoker
patients.Height = Height 
patients.Weight = Weight 
patients.BP = BloodPressure

% From a file
patients = readtable('patients.csv')

%% Extracting elements of table
% Extract column with table_name.variable_name
patients.Age 

% Variable add
patients.IMC = (patients.Weight) ./ (patients.Height).^2 

% Extract with () operator
patients(1,[1 2])
patients(:,[4 5 6])

% Extract with {}
patients{:,[4 5 6]}
patients{:,{'Height','Weight','IMC'}}



%% Sort function
sortrows(patients,{'Age','Height'})


