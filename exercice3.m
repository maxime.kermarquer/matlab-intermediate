
%% Load data
load gPrices

%% Choose a country (INPUTDLG)
myctry %

% To match the name of the country to its index 
idx = strcmp(myctry,country);

% Extract data for the chosen country.


%% Display average gas price


%%  Make plot
plot(Year,myPrices,'o-')
xlabel('Year')
ylabel([myctry,' prices $/gallon'])
title(['Gas prices for ' myctry ' from 1990 to 2015'])
