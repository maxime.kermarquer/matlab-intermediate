Temp_20220816 = struct ('date','2022-08-16','temp',[17 30 25])
Temp_20220817 = struct ('temp', 28, 'date','2022-08-17') 
Temp_20220818 = struct ('date','2022-08-18', 'temp', [15 28 32 29 25])

Temp_august = [Temp_20220816 ; Temp_20220817 ; Temp_20220818 ] % You can aggregate struct that have the same fields even in different order

% Access by field
Temp_august.date
Temp_august.temp